<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <meta name="referrer" content="origin-when-cross-origin" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A single file PHP script to generate long shadow avatars">
    <meta name="theme-color" content="#9437ff">
    <meta property="og:title" content="Invatar - long-shadow avatar generator" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<? echo $_SERVER['REQUEST_URI'] ?>" />
    <meta property="og:description" content="A single file PHP script to generate long shadow avatars" />
    <meta property="og:image" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>/img/hi" />

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@zohaibility">
    <meta name="twitter:creator" content="@zohaibility">
    <meta name="twitter:url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>">
    <meta name="twitter:title" content="Flat Long Shadows for your avatars">
    <meta name="twitter:description" content="A single file PHP script to generate long shadow avatars">
    <meta name="twitter:image" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>/img/hi">
    <title>Invatar - Long shadow initials avatar</title>

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">

    <!-- CSS Reset -->
    <link rel="stylesheet" href="//cdn.rawgit.com/necolas/normalize.css/master/normalize.css">

    <!-- Milligram CSS minified -->
    <link rel="stylesheet" href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css">
</head>
<body style="padding: 15px;">
    <h1>invatar</h1>
    <h3>single file PHP long-shadow avatar generator</h3>

    <div id="root" class="row" style="display: none; width: 98%; margin: 0 auto;" m-show="appReady">

        <div class="column">
            <center m-show="appReady" m-show="imageUri">
                <div style="max-width: 512px;">
                    <label>URL</label>
                    <pre><code> http://<?php echo $_SERVER['HTTP_HOST']; ?>{{imageUri}}</code></pre>
                </div>
                <a m-show="loaded" href="{{imageUri}}" target="_blank">
                    <img m-literal:alt="text" m-literal:src="imageUri" m-on:load="imageLoaded" style="border-radius: 5px"/><br/>
                    Open in new tab
                </a>
                <div m-show="!loaded" m-literal:alt="text" m-literal:src="imageUri">
                    <img src="https://media.giphy.com/media/3ornjOID5ncUy9xohW/giphy.gif" alt="" height="{{size}}" />
                    <br/>
                     Hold on...
                </div>
            </center>
        </div>

        <form class="column">
                <fieldset>
                    <label for="textField">Text</label>
                    <input placeholder="Text" id="textField" m-on:input="reloadImage" m-model="text" type="text" />

                    <label for="imageSizeField">Image size {{size}} (px)</label>
                    <input placeholder="Image size" id="imageSizeField" m-model="size" type="range" max="512" min="64" m-on:change="reloadImage" />

                    <label for="shadowType">Long Shadow:</label>
                    <select id="shadowType" m-model="slopes" m-on:change="reloadImage">
                        <option value="0,0">None</option>
                        <option value="1,1">Bottom Right</option>
                        <option value="1,-1">Top Right</option>
                        <option value="-1,1">Bottom Left</option>
                        <option value="-1,-1">Top Left</option>
                    </select>

                    <label for="font">Font style</label>
                    <select m-model="font" id="font" m-on:change="reloadImage">
                        <option value="n">Noto Sans (Multilingual)</option>
                        <option value="p">PT Sans Narrow</option>
                        <option value="g">Gidole</option>
                        <option value="s">Adobe Source Sans</option>
                    </select>

                    <label for="enableBg">
                        <input type="checkbox" m-model="customBackground" m-on:change="reloadImage" id="enableBg" /> Custom background color
                    </label>
                    <input m-if="customBackground" placeholder="Color" m-model="bgColor" m-on:change="reloadImage" type="color"/>
                    <br />

                    <label for="enableFg">
                        <input type="checkbox" m-model="customForeground" m-on:change="reloadImage" id="enableFg" /> Custom foreground color
                    </label>
                    <input m-if="customForeground" placeholder="Color" m-model="fgColor" m-on:change="reloadImage" type="color"/>
                    <br />

                    <label for="enableSh">
                        <input type="checkbox" m-model="customShadow" m-on:change="reloadImage" id="enableSh" /> Custom shadow color
                    </label>
                    <input m-if="customShadow" placeholder="Color" m-model="shColor" m-on:change="reloadImage" type="color"/>
                    <br />
                </fieldset>
        </form>

    </div>

    <div style="width: 300px; margin: 0 auto;">
        <!-- AddToAny BEGIN -->
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_google_plus"></a>
        <a class="a2a_button_hacker_news"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
        <!-- AddToAny END -->
    </div>

    <h2>story and inspiration</h2>
    <p>
        I recently saw <a href="https://news.ycombinator.com/item?id=15422995">Hacker news</a> post,
        thought to my self "hey I wrote a way cooler script for initials image generation when it was not so cool",
        so I decided to dig it out from my gists and share it with the world.
        I have put together <em>this single file</em> long shadow avatar generator demo (~350 LoC including comments).
        The technique used is pretty basic and works pretty well for any font.
    </p>

    <h2>url pattern</h2>
    <p>
      It's dead simple:
    </p>
    <pre><code>http://<?php echo $_SERVER['HTTP_HOST']; ?>/img/{text}/{size}/{background_color}</code></pre>
    <p>
      Here:
      <ul>
        <li><strong>text</strong> - is required, only first two characters shall be used for initials</li>
        <li><strong>size</strong> - is optional and max value can be 512</li>
        <li><strong>background_color</strong> - is optional if not specified a deterministic color from pallete will be used</li>
      </ul>

      Additionally you can specify query parameters:
      <ul>
        <li><strong>sh</strong> - hex value for shadow color (by default it's darken value of background color)</li>
        <li><strong>fg</strong> - hex value for foreground/text color (by default it's lighten value of background color)</li>
        <li><strong>slope_x</strong> - A value between +1 to -1 controlling slope of shadow along x-axis</li>
        <li><strong>slope_y</strong> - A value between +1 to -1 controlling slope of shadow along y-axis</li>
        <li><strong>font</strong> - A font alias configured in script (see source for more details)</li>
      </ul>
    </p>

    <h3>about this demo</h3>
    <p>
        The above demo uses PHP 7, GD2, and
        <a href="https://www.google.com/get/noto/">Google Noto</a>,
        <a href="https://fonts.google.com/specimen/PT+Sans+Narrow">PT Sans Narrow</a>,
        <a href="http://gidole.github.io/">Gidole</a> and
        <a href="https://github.com/adobe-fonts/source-sans-pro/releases">Source Sans</a>
        font to generate text avatars.
    </p>
    <p>
        Please checkout links for each font for their respective copyright information.
        <em>
          I have tried to use opensource/public domain fonts.
          Please check with respective font owners before using the images/fonts in your commercial project.
        </em>
    </p>

    <h3>source code</h3>
    <p>
        Feel free to contribute or report issues on: <br />
        <a class="button button-outline" href="https://gitlab.com/maxpert/invatar">Git Repo</a>
    </p>

    <?php
    if (file_exists('./page_footer.php')) {
      require './page_footer.php';
    }
    ?>
    </body>

<script src="https://unpkg.com/moonjs"></script>
<script>
    var Defaults = {
      size: 128,
      font: 'n',
      slopes: '1,1'
    };

    var app = new Moon({
        el: "#root",
        data: {
            text: "∆",
            slopes: "1,1",
            font: "n",
            currentSize: 128,
            customForeground: false,
            fgColor: '#ffffff',
            customShadow: false,
            shColor: '#000000',
            customBackground: false,
            bgColor: "#9437ff",

            appReady: true,

            loaded: false,
            imageUri: null
        },
        hooks: {
            init: function() {
                this.callMethod('reloadImage');
            }
        },
        methods: {
            imageLoaded: function(e) {
                this.set('loaded', true);
            },

            reloadImage: function() {
                var me = this;
                if (this.timer) {
                    window.clearTimeout(this.timer);
                }

                me.set('loaded', false);
                this.timer = window.setTimeout(function() {
                    me.set('imageUri', me.callMethod('getImageUrl'));
                }, 500);
            },

            getImageUrl: function() {
                var ret = '/img/' + encodeURIComponent(this.get("text") || ' ');
                var queryParams = [];
                var slopes = this.get("slopes").split(',');

                if (this.get('size') != Defaults.size || this.get('customBackground')) {
                  ret += '/' + this.get('size');
                }

                if (this.get('customBackground')) {
                    ret += "/" + encodeURIComponent(this.get('bgColor').substring(1));
                }

                if (this.get('font') != Defaults.font) {
                  queryParams.push('font=' + this.get('font'));
                }

                if (this.get('customForeground')) {
                    queryParams.push('fg=' + encodeURIComponent(this.get('fgColor').substring(1)));
                }

                if (this.get('customShadow')) {
                    queryParams.push('sh=' + encodeURIComponent(this.get('shColor').substring(1)));
                }

                if (this.get('slopes') != Defaults.slopes) {
                    queryParams.push("slope_x=" + slopes[0]);
                    queryParams.push("slope_y=" + slopes[1]);
                }

                return ret + (queryParams.length ? '?' + queryParams.join('&') : '');
            }
        },
        computed: {
            size: {
                get: function() {
                    return this.get('currentSize');
                },

                set: function(val) {
                    if (val > 512) {
                        val = 512
                    }

                    this.set('currentSize', val);
                }
            }
        }
    });
</script>
</html>
